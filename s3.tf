resource "aws_s3_bucket" "onebucket" {
     bucket = "mbr1210"

   acl = "private"

   versioning {

      enabled = true

   }

   tags = {

     Name = "Bucket1"

     Environment = "Test"

   }

}


terraform {

    cloud {

        organization = "bharad1210"

        workspaces {

            name = "terraform"

        }

    }

}